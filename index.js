const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server, { wsEngine: 'ws' });

const HOST = process.env.HOST || '0.0.0.0';
const PORT = process.env.PORT || 5000;

app.use(express.static('public'));

io.on('connection', (socket) => {
  console.log('Player connected');
  socket.emit('connected');

  socket.on('event name', () => {
    console.log('Event recieved');
  });

  socket.on('disconnect', () => {
    console.log('Player disconnected');
  });
});

server.listen(PORT, HOST, () => {
  console.log(`Server running @ http://${HOST}:${PORT}`);
});
